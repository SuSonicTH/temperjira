// ==UserScript==
// @name         JiraSprintStatistics
// @namespace    http://tampermonkey.net/
// @version      1.0.6
// @description  show sprint statistics
// @author       Michael Wolf
// @updateURL    https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraSprintStatistics.js
// @downloadURL  https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraSprintStatistics.js
// @match        https://jira.three.com/secure/RapidBoard.jspa?rapidView=8703*
// @require      https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js
// @require      https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js
// @require      https://raw.githubusercontent.com/zenorocha/good-listener/master/dist/good-listener.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/UiUtilities.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraAPI.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraUiDialog.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraUi.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/HtmlTable.js
// ==/UserScript==


function showStatistics() {
    jiraGet('https://jira.three.com/rest/api/2/search/?maxResults=250&fields=assignee,estimate,status,customfield_10002&jql=project=OC+AND+issuetype+in+(Bug,Story,Task)+AND+sprint+in+openSprints()', "", showDialog, showError);
}

function showDialog(data) {
    let list = groupByAssignee(data);

    let dialog = new Dialog("Sprint Statistics", "statistics-dialog");
    dialog.html += createTable(list);
    dialog.end()
    $('body').append(dialog.html);
}

var unassignedImage = "https://jira.three.com/secure/projectavatar?pid=12500&avatarId=18159";

function groupByAssignee(data) {
    let grouped = {}
    let list = []
    for (var i = 0; i < data.issues.length; i++) {
        let issue = data.issues[i];
        let assignee = issue.fields.assignee;
        let assigneeName = assignee == null ? "Unassigned" : assignee.displayName;
        let assigneeImage = assignee == null ? unassignedImage : assignee.avatarUrls["32x32"];

        console.log(assignee + "," + assigneeName + "," + assigneeImage);
        let status = issue.fields.status.name;
        let estimate = issue.fields.customfield_10002;
        if (!grouped[assigneeName]) {
            let image = '<img src="' + assigneeImage + '" alt="' + assignee + '">'
            grouped[assigneeName] = { "Assignee": image, "Done": 0, "In Progress": 0, "External Blocked": 0, "To Do": 0, "Estimate Done": 0, "Estimate To Do": 0 };
            list.push(grouped[assigneeName]);
        }
        let item = grouped[assigneeName];
        item[status]++;
        if (status == "Done") {
            item["Estimate Done"] += estimate;
        } else {
            item["Estimate To Do"] += estimate;
        }
    }
    list.sort(function (a, b) {
        return b["Estimate Done"] - a["Estimate Done"];
    });
    list.push(getSummary(list));
    return list;
}

function getSummary(list) {
    let sum = { "Assignee": "SUM", "Done": 0, "In Progress": 0, "External Blocked": 0, "To Do": 0, "Estimate Done": 0, "Estimate To Do": 0 };
    for (let i = 0; i < list.length; i++) {
        sum["Done"] += list[i]["Done"];
        sum["In Progress"] += list[i]["In Progress"];
        sum["External Blocked"] += list[i]["External Blocked"];
        sum["To Do"] += list[i]["To Do"];
        sum["Estimate Done"] += list[i]["Estimate Done"];
        sum["Estimate To Do"] += list[i]["Estimate To Do"];
    }
    return sum;
}

function createTable(list) {
    return new HtmlTable("statistics-table", "100%")
        .setHeader(['Assignee',
            JiraStatusTag.success("Estimate Done", true),
            JiraStatusTag.new("Estimate To Do", true),
            JiraStatusTag.success("Done", true),
            JiraStatusTag.inProgress("In Progress", true),
            JiraStatusTag.error("External Blocked", true),
            JiraStatusTag.new("To Do", true)])
        .setDataAssoc(list,
            ["Assignee",
                "Estimate Done",
                "Estimate To Do",
                "Done",
                "In Progress",
                "External Blocked",
                "To Do"])
        .setTdStyle("border: 1px solid black; text-align: center; padding: 10px;")
        .setEqualWidths("14.28%")
        .create()
        .html;
}

function showError(text) {
    $.notify("Could not get jira data: " + text, "error");
}

$('body').on('click', '#statistics-dialog-cancel', function () { $('#statistics-dialog').remove() });
$('body').on('click', '#statistics', showStatistics);

$(document).ajaxComplete(function () {
    if (!$('#statistics').length) {
        $(".active-sprint-lozenge").after('<a href="#" id="statistics">' + JiraStatusTag.new("Statistics", true) + '</a>');
    }
})();
