class Dialog {
    constructor(title, id) {
        this.id = id;
        this.html =
            '<div id="' + id + '" class="jira-dialog box-shadow jira-dialog-open popup-width-custom jira-dialog-content-ready" style="width: 810px; margin-left: -406px; margin-top: -271px;">' +
            '    <div class="jira-dialog-heading">' +
            '        <h2 title="' + title + '">' + title + '</h2>' +
            '    </div>' +
            '    <div class="jira-dialog-content">' +
            '        <div class="qf-container">' +
            '            <div class="qf-unconfigurable-form">' +
            '                <form action="#" name="jiraform" class="aui">' +
            '                    <div class="form-body" style="max-height: 1393px;">' +
            '                        <div class="content">';
    }

    addInput(label, value, id) {
        this.html = this.html +
            '                            <div class="field-group">' +
            '                                <label for="' + id + '">' + label + '</label>' +
            '                                <input' +
            '                                    class="text long-field"' +
            '                                    id="' + id + '"' +
            '                                    name="' + id + '"' +
            '                                    type="text"' +
            '                                    value="' + value + '"' +
            '                                >' +
            '                            </div>';
    }

    addListBox(label, options, selected, id) {
        this.html = this.html +
            '                            <div class="field-group">' +
            '                                <label for="' + id + '">' + label + '</label>' +
            '                                <select class="select" id="' + id + '" name="' + label + '" value="' + selected + '">';
        for (let option of options) {
            this.html = this.html +
                '                                    <option value="' + option + '">' + option + '</option>';
        }

        this.html = this.html +
            '                                </select>' +
            '                            </div>';
    }

    addCheckedInput(label, value, id, chkText, checked) {
        this.html = this.html +
            '                            <div class="field-group">' +
            '                                <label for="' + id + '">' + label + '</label>' +
            '                                <input' +
            '                                    class="text long-field"' +
            '                                    id="' + id + '"' +
            '                                    name="' + id + '"' +
            '                                    type="text"' +
            '                                    value="' + value + '"' +
            '                                >' +
            '                                <input type="checkbox" id="' + id + '-check" name="' + id + '-check" ' + checked + '>' + chkText +
            '                            </div>';
    }

    end(create = false) {
        this.html = this.html +
            '                    <div class="buttons-container form-footer">' +
            '                        <div class="buttons">' +
            '                            <span class="throbber"></span>'
        if (create) {
            this.html = this.html +
                '                            <a' +
                '                                href="#"' +
                '                                accesskey="s"' +
                '                                title="Press Alt + s to submit"' +
                '                                class="submit"' +
                '                                id="' + this.id + '-submit"' +
                '                            >Create</a>'
        }
        this.html = this.html +
            '                            <a' +
            '                                href="#"' +
            '                                accesskey="`"' +
            '                                title="Press Alt+` to cancel"' +
            '                                class="cancel"' +
            '                                id="' + this.id + '-cancel"' +
            '                            >Cancel</a>' +
            '                        </div>' +
            '                    </div>' +
            '                </form>' +
            '            </div>' +
            '        </div>' +
            '    </div>' +
            '</div>';
    }
}