
function jiraCall(method, url, data, okStatus, onOK, onError) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == okStatus) {
                var response = JSON.parse(xhr.responseText);
                if (onOK) {
                    onOK(response, xhr);
                }
            } else {
                if (OnError) {
                    onError(response, xhr)
                }
            }
        }
    }
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(data));
}

function jiraGet(url, data, onOK, onError) {
    jiraCall("GET", url, data, 200, onOK, onError);
}

function jiraPost(url, data, okStatus, onOK, onError) {
    jiraCall("POST", url, data, okStatus, onOK, onError);
}

function jiraCreate(url, data, onOK, onError) {
    jiraCall("POST", url, data, 201, onOK, onError);
}

var jiraUserName = "";
{
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            jiraUserName = xhr.getResponseHeader("X-AUSERNAME");
        }
    }
    xhr.open('GET', '/rest/api/2/mypermissions/', true);
    xhr.send();
}

function getJiraUserName() {
    return jiraUserName;
}

function createSubTasks(project, parent, summary, userName, onOK) {
    if (summary.trim().length == 0) {
        if (onOK) {
            onOK();
        }
        return;
    }

    jiraCreate('/rest/api/2/issue/',
        {
            "fields":
            {
                "project":
                {
                    "key": project
                },
                "parent":
                {
                    "key": parent
                },
                "summary": summary,
                "issuetype":
                {
                    "id": "10101"
                },
                "assignee": {
                    "name": userName,
                }
            }
        },
        function (response) {
            $.notify("Successfully created: " + response.key + " - " + summary, "success");
            if (onOK) onOK(response);
        }, function () {
            $.notify("Could not create subtask\nResponse: " + this.responseText, "error");
        });
}

function createSubTasksList(project, parent, values, users, current, last) {
    createSubTasks(project, parent, values[current], users[current], function () {
        if (current + 1 <= last) {
            createSubTasksList(project, parent, values, users, current + 1, last);
        } else {
            $.notify("Created subtasks - reloading page", "success");
            location.reload();
        }
    });
}
