// ==UserScript==
// @name         JiraTools
// @namespace    http://tampermonkey.net/
// @version      2.8.2
// @description  Adds handy dropdown to toolbar in the issue browser
// @author       Michael Wolf
// @updateURL    https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraTools.js
// @downloadURL  https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraTools.js
// @match        https://jira.three.com/browse/*
// @match        https://jiratest.three.com/browse/*
// @require      https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js
// @require      https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js
// @require      https://raw.githubusercontent.com/zenorocha/good-listener/master/dist/good-listener.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/UiUtilities.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraAPI.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraUiDropDown.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraUiDialog.js
// ==/UserScript==

var createSubtaskCount = 10;
var users = ['sawyerri', 'skelegu', 'stadlest', 'wolfmi', 'maritan', 'akiplial'];
var blackMedJobDetails = '\nh4. Job Configuration\n\n' +
    '||Name\\*| |\n' +
    '||Source\\*| |\n' +
    '||Source Password| |\n' +
    '||Source Key| |\n' +
    '||Source Pattern\\*| |\n' +
    '||Source Name Script| |\n' +
    '||Source Rename Script| |\n' +
    '||Source Delay| |\n' +
    '||Destination\\*| |\n' +
    '||Destination Password| |\n' +
    '||Destination Key| |\n' +
    '||Destination Rename Script| |\n' +
    '||Processing Script| |\n' +
    '||Transfer Suffix|.tmp |\n' +
    '||Schedule\\*| |\n' +
    '||Archive Name| |\n' +
    '||Output Archive Name| |\n' +
    '||Delete Source|(/)|\n' +
    '||Merge All Files| |\n' +
    '||Only First Header| |\n' +
    '||Zip Output| |\n' +
    '||Un Zip Input| |\n' +
    '||Not Collected Log Level| None |\n';

var blackMedJobDetailsAligned = '\nh4. Job Configuration\n\n' +
    '||Name\\*                    | |\n' +
    '||Source\\*                  | |\n' +
    '||Source Password           | |\n' +
    '||Source Key                | |\n' +
    '||Source Pattern\\*          | |\n' +
    '||Source Name Script        | |\n' +
    '||Source Rename Script      | |\n' +
    '||Source Delay              | |\n' +
    '||Destination\\*             | |\n' +
    '||Destination Password      | |\n' +
    '||Destination Key           | |\n' +
    '||Destination Rename Script | |\n' +
    '||Processing Script         | |\n' +
    '||Transfer Suffix           |.tmp |\n' +
    '||Schedule\\*                | |\n' +
    '||Archive Name              | |\n' +
    '||Output Archive Name       | |\n' +
    '||Delete Source             |(/)|\n' +
    '||Merge All Files           | |\n' +
    '||Only First Header         | |\n' +
    '||Zip Output                | |\n' +
    '||Un Zip Input              | |\n' +
    '||Not Collected Log Level   | None |\n';

function removeSubTasksDialog() {
    $('#subtask-dialog').remove()
}

function submitCreateSubTasksDialog(project, parent, userName) {
    let values = [];
    let users = [];
    for (let i = 1; i <= createSubtaskCount; i++) {
        values[i] = $('#subtask' + i).attr("value").trim()
        users[i] = $('#subtask' + i + '-check').is(':checked') ? userName : ""
    }
    createSubTasksList(project, parent, values, users, 1, createSubtaskCount);
    removeSubTasksDialog();
}

function createSubTaskDialog(values, users) {
    let dialog = new Dialog("Create subtasks for " + $('#key-val').text(), "subtask-dialog");
    for (let i = 1; i <= createSubtaskCount; i++) {
        dialog.addCheckedInput("Subtask " + i, values[i - 1], "subtask" + i, "assign to me", users[i - 1] ? "checked" : "");
    }
    dialog.end(true)
    $('body').append(dialog.html);
}

var dropdown = new DropDown("Tools", "jira-issue-tools");
dropdown.addItem('Copy Text', 'Copy issue key and summary', function() { return $('#key-val').text() + " " + $('#summary-val').text(); }, true);
dropdown.addItem('Testpage Title', 'Copy testpage title', function() { return "Test Report - " + $('#key-val').text() + " - " + $('#summary-val').text(); }, true);
dropdown.addItem('Create Subtasks', 'Create subtasks tasks', function() { createSubTaskDialog(["", "", "", "", "", "", "", "", "", ""], [false, false, false, false, false, false, false, false, false, false]); }, false);
dropdown.addItem('Standard Subtasks', 'Create Implement, Test and Review tasks', function() { createSubTaskDialog(["Implement", "", "", "", "", "", "", "", "Test", "Review"], [false, false, false, false, false, false, false, false, false, false]); }, false);
dropdown.addItem('BlackMed CopyJob', 'Copy BlackMed CopyJob description', function() { return blackMedJobDetails; }, true);
dropdown.addItem('BlackMed CopyJob Aligned', 'Copy BlackMed CopyJob description fields are aligned', function() { return blackMedJobDetailsAligned; }, true);
dropdown.end();

$('body').on('click', '#subtask-dialog-submit', function() { submitCreateSubTasksDialog("OC", $('#key-val').text(), getJiraUserName()); });
$('body').on('click', '#subtask-dialog-cancel', removeSubTasksDialog);

$(document).ajaxComplete(function() {
    dropdown.addToPage('#opsbar-opsbar-operations', 'opsbar-opsbar-jiraTools')
})();
