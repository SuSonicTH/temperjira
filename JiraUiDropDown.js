class DropDown {
    constructor(text, id) {
        this.html = 
            '<a href="#" id="' + id + '_more" aria-owns="' + id + '_more_drop" aria-haspopup="true" class="toolbar-trigger aui-button aui-style-default aui-dropdown2-trigger">\n' +
            '<span class="dropdown-text">' + text + '</span></a>\n' +
            '<div id="' + id + '_more_drop" class="aui-style-default aui-dropdown2">\n' +//New section
            '<div class="aui-dropdown2-section">\n' +
            '<ul>\n\n';
    }

    addSection() {
        this.html +=
            '</ul></div>\n\n' + //close open section
            '<div class="aui-dropdown2-section">\n' +
            '<ul>\n\n';
    }

    addItem(name, title, func, clipboard) {
        var id = name.replace(/ /g, "-");
        this.html +=
            '<li class = "aui-list-item">\n' +
            '<a id="' + id + '" href="#" title="' + title + '">\n' +
            '<span class="trigger-label">' + name + '</span>\n' +
            '</a></li>\n';
        if (func) {
            if (clipboard) {
                addClipboardTrigger('#' + id, func);
            } else {
                listen('#' + id, 'click', func);
            }
        }
    }

    end() {
        this.html = this.html + '</ul></div></div>\n';
    }

    addToPage(isertAfter, id) {
        if (!$('#' + id).length) {
            $(isertAfter).append('<div id="' + id + '" class="aui-buttons pluggable-ops">' + dropdown.html + '</div>');
        }
    }
}
