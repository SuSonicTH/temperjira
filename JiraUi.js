class JiraStatusTag{
    static lozenge(type,text,subtile){
        return '<span class="aui-lozenge '+type+ (subtile?' aui-lozenge-subtle':"") +'">'+text+'</span>';
    }

    static new(text,subtle){
        return this.lozenge('',text,subtle);
    }

    static success(text,subtle){
        return this.lozenge('aui-lozenge-success',text,subtle);
    }
    static error(text,subtle){
        return this.lozenge('aui-lozenge-error',text,subtle);
    }
    static removed(text,subtle){
        return this.lozenge('aui-lozenge-removed',text,subtle);
    }
    static inProgress(text,subtle){
        return this.lozenge('aui-lozenge-inprogress',text,subtle);
    }
    static complete(text,subtle){
        return this.lozenge('aui-lozenge-complete',text,subtle);
    }
    static moved(text,subtle){
        return this.lozenge('aui-lozenge-moved',text,subtle);
    }
}
