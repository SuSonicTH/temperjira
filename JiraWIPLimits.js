// ==UserScript==
// @name         JiraWIPLimits
// @namespace    http://tampermonkey.net/
// @version      1.0.9
// @description  show WIP limits
// @author       Michael Wolf
// @updateURL    https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraWIPLimits.js
// @downloadURL  https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraWIPLimits.js
// @match        https://jira.three.com/secure/RapidBoard.jspa?rapidView=8703*
// @require      https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js
// @require      https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js
// @require      https://raw.githubusercontent.com/zenorocha/good-listener/master/dist/good-listener.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/UiUtilities.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraAPI.js
// @require      https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraUi.js
// ==/UserScript==

var inProgressWIP = 10;
var externalBlockedWIP = 8;

var updateInProgress;
var coachUser = 'divitost';

function getWIPLimits() {
    if (!updateInProgress) {
        inProgress = true;
        jiraGet('https://jira.three.com/rest/api/2/search/?maxResults=250&fields=status,&jql=project=OC+AND+issuetype+in+(Bug,Story,Task)+AND+sprint+in+openSprints()AND+assignee+not+in+(' + coachUser + ')', "", showWIPLimits, showError);
    }
}

function showError(text) {
    $.notify("Could not get jira data: " + text, "error");
}

function showWIPLimits(data) {
    let sum = { "Done": 0, "In Progress": 0, "External Blocked": 0, "To Do": 0 };
    for (var i = 0; i < data.issues.length; i++) {
        let status = data.issues[i].fields.status.name;
        sum[status]++;
    }
    if ($('#wipLimits').length) {
        $('#wipLimits').remove();
    }

    $(".subnavigator-title")
        .after('<div id="wipLimits">' +
            "&nbsp;" +
            getInWIPDisplay("WIP Progress", sum['In Progress'], inProgressWIP) +
            "&nbsp;" +
            getInWIPDisplay("WIP Blocked", sum['External Blocked'], externalBlockedWIP) + '</div>'
        );
    updateInProgress = false;
}

function getInWIPDisplay(text, count, limit) {
    let display = text + " " + count + "/" + limit;
    if (count > limit) {
        return JiraStatusTag.error(display, false);
    }
    return JiraStatusTag.success(display, false);
}

$(document).ajaxComplete(function () {
    if (!$('#wipLimits').length) {
        getWIPLimits();
    }
})();
