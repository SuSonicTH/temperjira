# Jira TamperMonkey scripts

this repository has libraries and scripts to add handy tools to Jira

To add the *JiraTools* script you have to have [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/) installed.
Once installed you can go to Tampermonkey -> Dashboard -> Utilities -> install from URL and paste URLs one after the oterh and press install.


https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraTools.js - gives the tools menu on top of an open jira item (copy test, copy testpage title,...)
https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraSprintStatistics.js - gives you the statistics on the backlog
https://gitlab.com/SuSonicTH/temperjira/-/raw/master/JiraWIPLimits.js - gives you the WIP limit in the sprint backlog.
