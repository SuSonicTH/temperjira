class HtmlTable {
    constructor(id, width) {
        this.id = id;
        this.width = width ? width : "";
        this.tdStyle = "";
        this.thStyle = "";
    }

    setHeader(header) {
        this.header = header;
        return this;
    }

    setTdStyle(style) {
        this.tdStyle = style;
        return this;
    }

    setThStyle(style) {
        this.thStyle = style;
        return this;
    }

    setDataAssoc(data, fields) {
        this.data = [];
        for (let r = 0; r < data.length; r++) {
            let row = [];
            for (let f = 0; f < fields.length; f++) {
                row.push(data[r][fields[f]]);
            }
            this.data.push(row);
        }
        return this;
    }

    setData(data) {
        this.data = data;
        return this;
    }

    setWidths(widths) {
        this.widths = widths;
        return this;
    }

    setEqualWidths(width) {
        this.equalWidths = width;
        return this;
    }

    createHeader() {
        if (this.header) {
            let html = '<tr>';
            for (let f = 0; f < this.header.length; f++) {
                html += '<th style="' + this.thStyle + '">' + this.header[f] + '</th>';
            }
            html += "</tr>";
            return html;
        }
    }

    create() {
        let html = '<table id="' + this.id + ' width="' + this.width + '"><tbody>';
        html += this.createHeader();
        for (let r = 0; r < this.data.length; r++) {
            html += "<tr>";
            let row = this.data[r];
            for (let f = 0; f < row.length; f++) {
                html += '<td style="' + this.tdStyle + '"';
                if (this.equalWidths) {
                    html += ' width="' + this.equalWidths + '"';
                } else if (this.widths) {
                    html += ' width="' + this.widths[f] + '"';
                }
                html += '>' + row[f] + '</td>';
            }
            html += "</tr>";
        }
        html += '</tbody></table>';
        this.html = html;
        return this;
    }
}
