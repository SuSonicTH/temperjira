function addClipboardTrigger(sel, func) {
    let clipboard = new Clipboard(sel, {
        text: function (trigger) {
            return func();
        }
    });
    clipboard.on('success', function (e) {
        $.notify("Copied to clipboard '" + func() + "'", "success");
    });
    clipboard.on('error', function (e) {
        $.notify("Access granted", "error");
    });
}